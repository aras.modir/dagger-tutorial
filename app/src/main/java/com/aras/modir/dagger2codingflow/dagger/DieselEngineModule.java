package com.aras.modir.dagger2codingflow.dagger;

import com.aras.modir.dagger2codingflow.car.DieselEngine;
import com.aras.modir.dagger2codingflow.car.Engine;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class DieselEngineModule {

    @Binds
    abstract Engine bindEngine(DieselEngine engine);

}
