package com.aras.modir.dagger2codingflow.car;


public interface Engine {

    void start();

}
