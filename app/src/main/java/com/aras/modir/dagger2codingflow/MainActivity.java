package com.aras.modir.dagger2codingflow;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.aras.modir.dagger2codingflow.car.Car;
import com.aras.modir.dagger2codingflow.dagger.CarComponent;
import com.aras.modir.dagger2codingflow.dagger.DaggerCarComponent;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {
    @Inject public Car car;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CarComponent component = DaggerCarComponent.create();
        component.inject(this);

        car.drive();
    }
}
