package com.aras.modir.dagger2codingflow.car;

import android.util.Log;

public class Tires {

    public static final String TAG = "Car";

    public void inflate() {
        Log.d(TAG, "Tires inflated");
    }
}
