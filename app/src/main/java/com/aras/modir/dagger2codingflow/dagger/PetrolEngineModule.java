package com.aras.modir.dagger2codingflow.dagger;

import com.aras.modir.dagger2codingflow.car.Engine;
import com.aras.modir.dagger2codingflow.car.PetrolEngine;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class PetrolEngineModule {

    @Binds
    abstract Engine bindEngine(PetrolEngine engine);

}
