package com.aras.modir.dagger2codingflow.car;

import android.util.Log;

import javax.inject.Inject;

public class Remote {
    public static final String TAG = "Car";

    @Inject
    public Remote() {
    }

    public void setListener(Car car) {
        Log.d(TAG, "remote connected ");
    }
}
