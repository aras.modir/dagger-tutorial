package com.aras.modir.dagger2codingflow.dagger;

import com.aras.modir.dagger2codingflow.MainActivity;
import com.aras.modir.dagger2codingflow.car.Car;

import dagger.Component;

@Component (modules = {WheelsModule.class, DieselEngineModule.class})
public interface CarComponent {

    Car getCar();

    void inject(MainActivity mainActivity);

}
